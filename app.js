const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config/config.js');
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "DELETE, PUT, UPDATE, HEAD, OPTIONS, GET, POST");
    next();
  });
const { options } = require('./swagger-options');

app.set('port', (process.env.PORT || global.appConfig.node_port));


app.use(bodyParser.json());

const jobsRoute = require('./controllers/api/jobs');
const categoriesRoute = require('./controllers/api/category');
const searchRoute = require('./controllers/api/search');
const generateRoute = require('./controllers/api/generator')
const nodemailer = require('./controllers/api/nodemailer')

app.use('/api/jobs', jobsRoute);
app.use('/api/categories', categoriesRoute);
app.use('/api/search', searchRoute);
app.use('/api/send', nodemailer);
app.use('/api/generator', generateRoute);
app.use('/logo', express.static('logo'));

const specs = swaggerJsdoc(options);
app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs)
);

app.get('/', function(request, response) {

    let result = 'App is running';
    response.send(result);
}).listen(app.get('port'), function() {
    console.log('App is running, server is listening on port ', app.get('port'));
});

let dbName = global.appConfig.database;


const connectLocal = 'mongodb://localhost:27017';
const connectRemote = 'mongodb+srv://arkhannanov:Victory20@iworkremotely-5l8jr.mongodb.net';

mongoose.connect(`${connectRemote}/${dbName}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true 
    })
    .catch(error => console.log(error));