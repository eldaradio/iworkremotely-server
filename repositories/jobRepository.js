const Job = require('../models/Job');

exports.createOne = async (data) => {
    return await (new Job(data)).save();
};
