const mongoose = require('mongoose');
const mongoosastic = require('mongoosastic');
const config = require('./../config/config.js');

const JobSchema = mongoose.Schema({
  companyName: {
    type: String,
    required: true,
    es_indexed: true,
  },
  position: {
    type: String,
    required: true,
    es_indexed: true,
  },
  location: {
    type: String,
    required: true,
    es_indexed: true,
  },
  category: {
    type: String,
    required: true,
    es_indexed: true,
  },
  logo: {
    type: String,
    required: false,
    es_indexed: true,
  },
  tags : {
    type: String,
    required: false,
    es_indexed: true,
  },
  salaryMin: {
    type: Number,
    required: true,
    es_indexed: true,
  },
  salaryMax: {
    type: Number,
    required: false,
    es_indexed: true
  },
  jobDescription: {
    type: String,
    required: false,
    es_indexed: true
  },
  email: {
    type: String,
    required: false,
    es_indexed: true,
  },
  appendJobDescription: {
    type: String,
    required: false,
    es_indexed: true,
  },
  websiteUrl: {
    type: String,
    required: false,
    es_indexed: true,
  },
  createdAt: {
    type: String,
    required: false,
    es_indexed: true,
  },
  updatedAt: {
    type: String,
    required: false,
    es_indexed: true,
  }

});

JobSchema.set('timestamps', true);
JobSchema.set('autoIndex', false);
JobSchema.index({'$**': 'text'});

JobSchema.plugin(mongoosastic, {
  "host": global.appConfig.elastic_host,
  "port": global.appConfig.elastic_port
});

let Job = mongoose.model('job', JobSchema)
/*
, stream = Job.synchronize()
, count = 0;

stream.on('data', function(err, doc){
  count++;
});
stream.on('close', function(){
  console.log('indexed ' + count + ' documents!');
});
stream.on('error', function(err){
  console.log(err.message)
  console.log(JSON.stringify(err));
});
*/
Job.createMapping((err, mapping) => {
  // if (err) console.log('err____________________', err);
  
  console.log('mapping created' +mapping);
});

module.exports = Job;