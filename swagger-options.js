
const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Iworkremotely API with Swagger",
      version: "0.1.0",
      description:
        "",
    },
    servers: [
      {
        // url: "http://localhost:8080",
        url: "/",
      },
    ],
  },
  // apis: ["./controllers/api/jobs.js"],
  apis: ["./paths.yaml"],
};

module.exports = { options }