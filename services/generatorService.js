const faker = require('faker')
const jobRepository = require('../repositories/jobRepository')

const n = 500;

exports.g = async function generateJobs() {
    const profiles = await Promise.all(
        Array(n).fill(null).map(genJob).map(jobRepository.createOne)
    );
}

function genJob() {
    return {
        companyName: faker.commerce.productName(),
        jobTitle: faker.random.arrayElement(["Программист C++", "Programmer JS", "Дизайнер", "Разработчик на React", "Программист", "Разработчик Unreal Engine", "Копирайт", "Разработчик на Node.js"]),
        jobDescription: faker.name.jobDescriptor(),
        category: faker.random.arrayElement(["Бизнес & Менеджмент", "Копирайтинг", "Поддержка клиентов", "Финансы & Юриспруденция", "Дизайн", "DevOps & Системное администрирование", "Продуктолог", "Программирование", "Продажи & Маркетинг"]),
        location: faker.address.country(),
        tags: faker.random.arrayElement(["react", "javascript", "html", "css", "oop", "full-time"]),
        salaryMin: faker.commerce.price(),
        salaryMax: faker.commerce.price(),
        position: faker.random.arrayElement(["react", "javascript", "html", "css", "oop", "full-time"]),
        appendJobDescription: faker.commerce.productDescription(),
        websiteUrl: "any",
        email: faker.internet.email(),
        logo : null,
        createdAt : new Date().toISOString(),
        updatedAt : new Date().toISOString(),
    };
}