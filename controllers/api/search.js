const express = require('express');
const router = express.Router();
const Job = require('./../../models/Job');
const {Client} = require('@elastic/elasticsearch');
const config = require('./../../config/config.js');


Job.createMapping({
    "analysis": {
        "analyzer": {
            "content": {
                "type": "custom",
                "tokenizer": "standart",
                "filter": ['lowercase', 'snowball', 'russian_stemmer', 'russian_keywords', 'russian_stop']
            }
        }
    }
}, (err, mapping) => {
    console.log('mapping created ' + mapping);
});

const elasticHost = global.appConfig.elastic_host;
const elasticPort = global.appConfig.elastic_port;

// const elasticClient = new Client({
//     node: {
//         url: new URL(`http://${elasticHost}:${elasticPort}`),
//         // headers: {
//         //     Authorization: 'Basic ZWxhc3RpYzplbGFzdGlj'
//         // }
//     },
//     rejectUnauthorized: true
// });

// const elasticClient = new Client({
//     host: `http://${elasticHost}:${elasticPort}`
// })

router.get('/', async (req, res) => {
    try {
        const elasticClient = new Client({
            node: `http://${elasticHost}:${elasticPort}`
        })

        let { query: keyword } = req.query;

        const size = req.query.size;
        const page = req.query.page;
        const newPage = page - 1;

        if (typeof keyword === 'string' && keyword !== '') {
            let listOfMappedFields = ['jobTitle', 'category', 'tags', 'jobDescription', 'jobType'];
            const { body } = await elasticClient.search({
                index: 'jobs',
                body: {
                    size: size,
                    from: size * newPage,
                    query: {
                        multi_match: {
                            query: keyword,
                            type: 'phrase_prefix',
                            fields: listOfMappedFields,
                            operator: 'and'
                        },
                    },
                },
            });

            let searchResult = body.hits.hits.map(hit => {
                const { _id, _source } = hit;
                return Object.assign(_source, { _id })
            });
            // for(let searchResults of searchResult ) {
            //     console.log(searchResults.createdAt)
            // }

            // function byField(field) {
            //     return (a, b) => a[field] > b[field] ? 1 : -1;
            //   }
            //   searchResult.sort(byField('createdAt'));
            //   searchResult.forEach(user => res.send(user.createdAt));
            //   searchResult.sort((prev, next) => {
            //     if ( prev.createdAt < next.createdAt ) return -1;
            //     if ( prev.createdAt < next.createdAt ) return 1;
            // })
            res.send(searchResult);
        } else {
            const { _id } = req.body;
            try {
                await Job
                    .find({ owner: _id })
                    .sort({ createdAt: -1 })
                    .skip(newPage * size)
                    .limit(Number(size))
                    .then(jobs => {
                        return res.json(jobs);
                    })
            } catch (err) {
                res.json({ message: err });
            }
        }
    } catch (error) {
        console.log(error)
        res.json({ error: true, message: error.message })
    }

});

router.get('/autocomplete', async (req, res) => {
    try {
        const elasticClient = new Client({
            node: `http://${elasticHost}:${elasticPort}`
        })

        // let keyword = req.body.query;
        let {query: keyword} = req.query;

        if (keyword !== '') {
            let listOfMappedFields = [
                'jobTitle',
                'category',
                'tags',
                'jobDescription',
                'jobType',
                'headOffice',
                'applyLink'
            ];
            const {body} = await elasticClient.search({
                index: 'jobs',
                body: {
                    query: {
                        multi_match: {
                            query: keyword,
                            type: 'phrase_prefix',
                            fields: listOfMappedFields,
                            operator: "and"
                        }
                    },
                    size: 2,
                    highlight: {
                        fields: {
                            jobTitle: {"pre_tags": "", "post_tags": ""},
                            category: {"pre_tags": "", "post_tags": ""},
                            tags: {"pre_tags": "", "post_tags": ""},
                            jobDescription: {"pre_tags": "", "post_tags": ""},
                            headOffice: {"pre_tags": "", "post_tags": ""},
                            jobType: {"pre_tags": "", "post_tags": ""},
                            region: {"pre_tags": "", "post_tags": ""},
                            applyLink: {"pre_tags": "", "post_tags": ""},
                        }
                    }
                }
            });
            console.log(body.hits.hits[0]);

            // Job.search({
            //     query: keyword
            // }, (err, results) => {
            //     if (err) console.log(err);
            //     console.log(results);
            // })

            const set = new Set();
            let searchResult = body.hits.hits.forEach(item => {

                for (const [field, words] of Object.entries(item.highlight)) {
                    words.forEach((value, index, array) => {

                        if (!set.has(value)) set.add(value);
                    })
                }
            });
            // res.send(searchResult);
            res.send([...set]);
            // res.send([]);
        } else {
            res.json([]);
        }
    } catch (e) {
        console.log(e)
        res.json({error: true, message: e.message})
    }

});

module.exports = router;
