const express = require('express');
const router = express.Router();
const generateService = require('../../services/generatorService')

router.post('/',  async (req, res, next) => {
    try {
        await generateService.g();
        res.json({ ok: true });
      } catch (error) {
        next(error);
      }
})

module.exports = router;