const express = require('express');
const router = express.Router();
const Job = require('../../models/Job');
const {Client} = require('@elastic/elasticsearch');
const upload = require('../../libs/upload');


router.post('/', upload.single('logo'), async (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "DELETE, PUT, UPDATE, HEAD, OPTIONS, GET, POST");
    console.log(req)
    const {
        body
    } = req;

    const jobObject = Object.assign({}, body)
    const job = new Job(jobObject);

    try {
        console.log(req.body);
        const savedJob = await job.save();
        savedJob.on('es-indexed', (err, result) => {
            console.log('result', result)
            console.log('indexed to elastic search');
        });
        res.json(savedJob);
    } catch (err) {
        res.json({message: err});
    }
});

router.post('/test', async (req, res) => {
    try {
        res.json("Alex privet");
    } catch (err) {
        res.json({message: err});
    }
});

router.get('/:jobId', async (req, res) => {
    console.log('get job')
    try {
        const job = await Job.findById(req.params.jobId);
        res.json({
            success: true,
            data: {
                job
            }
        });
    } catch (err) {
        res.json({message: err});
    }
});
router.get('/', async (req, res) => {
    const { _id } = req.body;
    const size = parseInt(req.query.size);
    const page = parseInt(req.query.page);
    const newPage = page - 1;
    if (req.query.page <= 0 || req.query.size <= 0) {
        res.json({message: 'Not valid page'})
    }
    try {
        await Job
        .find({})
        .sort({createdAt : 1})
        .skip(newPage * size)
        .limit(size)
        .then(jobs => {
            return res.json(jobs);
        })
    } catch (err) {
        res.json({message: err});
    }
});

router.put('/apply/:jobId', async (req, res) => {
    try {
        console.log('/apply/:jobId')
        const job = await Job.findById(req.params.jobId);
        job.clicks = Number.isInteger(job.clicks) ? job.clicks + 1 : 1;
        job.save();
        res.json({
            success: true,
            data: {
                job
            }
        });
    } catch (err) {
        res.json({message: err});
    }
});

router.put('/show/:jobId', async (req, res) => {
    try {
        console.log('/show/:jobId')
        const job = await Job.findById(req.params.jobId);
        job.views = Number.isInteger(job.views) ? job.views + 1 : 1;
        job.save();
        console.log(job)
        res.json(job);
    } catch (err) {
        res.json({message: err});
    }
});

router.delete('/:jobId', async (req, res) => {
    try {
        const elasticPort = global.appConfig.elastic_port;
        const elasticClient = new Client({node: "http://localhost:" + elasticPort});
        const removedJob = await Job.remove({_id: req.params.jobId});
        const removeFromElastic = await elasticClient.delete({
            index: 'jobs',
            type: '_doc',
            id: req.params.jobId,
        });
        res.json(removedJob);
    } catch (err) {
        res.json({message: err});
    }
});

module.exports = router;