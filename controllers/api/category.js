const express = require('express');
const router = express.Router();
const Job = require('../../models/Job');
let moment = require('moment');

const categories = {
    'all-others': 'Все другие',
    'business-management': 'Бизнес & Менеджмент',
    copyrighting: 'Копирайтинг',
    'customer-support': 'Поддержка клиентов',
    'finance-legal': 'Финансы & Юриспруденция',
    design: 'Дизайн',
    'devops-system-administration': 'DevOps & Системное администрирование',
    product: 'Продуктолог',
    programming: 'Программирование',
    'sales-marketing': 'Продажи & Маркетинг',
}

router.get('/:category_name', async(req, res) => {
    try {
        const { category_name: category } = req.params;
        const categoryRus = categories[category];
        const size = parseInt(req.query.size);
        const page = parseInt(req.query.page);
        const newPage = page - 1;
        if (req.query.page <= 0 || req.query.size <= 0) {
            res.json({message: 'Not valid page'})
        }
        const allJobs = await Job
            .find({
                category: categoryRus
            })
            .sort({ createdAt: 1})
            .limit(size)
            .skip(newPage * size)
        const allJobsAmount = await Job
            .countDocuments({
                category: categoryRus
            });
        const all = allJobsAmount / size
        res.json({
            JobsPageCount: Math.ceil(all),
            Jobs: allJobs
        });
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;